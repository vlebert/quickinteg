# Utilisation des scripts sous Qgis 3.x

Les scripts sont encapsulés sous la forme d'outils Qgis "processing". Pour y accéder il suffit d'ouvrir la boîte à outil Qgis :

![image](static/img/qgis_processing_toolbox_open.png)

Les scripts sont ensuite listés dans la rubrique **Scripts/Tactis_Quickinteg** :

![image](static/img/qgis_processing_scripts.png)

----

## import_folder_to_spatialite

C'est l'outil à destination des personnes qui auront à intégrer les livrables.

![image](static/img/quickinteg_ui_import_folder_to_spatialite.png)

Le dossier source doit être un dossier comprenant des fichiers SHP et CSV

* Une case à cocher permet d'intégrer les CSV également (cochée par défaut mais possible de la décocher)
* Une case à cocher permet une recherche récursive dans les sous-dossiers. En mode récursif, la valeur de préfixe (voir plus bas) sera ignorée et remplacée par le chemin de chaque sous-dossier.

Deux mode d'intégration sont possible :

* Le mode **CREATE** : une base de donnée spatialite "base.sqlite" sera créé à la racine du dossier source. Les tables sont crées par défaut à partir de shp et csv source.
* Le mode **APPEND** : ce mode permet de faire une importation dans une base de donnée existante. Ce mode permet donc :
    * De faire de la consolidation de livrables (plusieurs livrables dans une même base cible)
    * De faire des contrôles si la base de destination est un _template_ paramétré avec Quickinteg (voir **template_create_from_cfg**)

En mode **APPEND** il faut donc également sélectionner la base de donnée de destination. Une case à cocher permet de faire l'export des vues de contrôle si la base est un _template_ paramétré pour quickinteg.

Enfin un champ texte permet d'ajouter un préfixe. Si une valeur est saisie, une colonne **"import_prefix"** sera créée sur chaque table avec cette valeur. Cela permet de faire du versionnage dans une base de donnée de consolidation de livrables, et de pouvoir supprimer un livrable de la base par la suite (voir [`delete_from_spatialite`](#delete_from_spatialite))

----

## delete_from_spatialite

Comme vu plus haut, **import_folder_to_spatialite** permet de faire de la consolidation de livrables dans une même base de donnée.

L'outil [`delete_from_spatialite`](#delete_from_spatialite) vient compléter cette fonctionnalité pour permettre la suppression d'un livrable de la base consolidée (pour le remplacer par une nouvelle version par exemple).

![image](static/img/quickinteg_ui_delete_from_spatialite.png)

Pour cela, il suffit de choisir la base de donnée spatialite à traiter et de préciser la valeur **import_prefix** à supprimer. Tous les enregistrement comprenant cette valeur seront effacés.

----

## template_create_from_cfg

Ce script permet de créer un **template** c'est à dire une base de donnée spatialite paramétrée grace à quickinteg de manière à effectuer des contrôles automatiques sur la donnée.

![image](static/img/quickinteg_ui_template_create_from_cfg.png)

Un chapitre du wiki est dédié à la création des templates.

----

## Backup et retour en arrière après une opération

A chaque utilisation des scripts [`delete_from_spatialite`](#delete_from_spatialite) ou **import_folder_to_spatialite**, une sauvegarde de la base de donnée est réalisée à la racine (fichier .sqlite_backup). Il est donc possible de revenir en arrière en cas d'erreur (suppression du .sqlite et renommage du .sqlite_backup).

!!! warning "Attention"
    Une seule backup est réalisée. Si deux opération sont réalisée à la suite, il ne sera possible d'annuler que la dernière opération.
