# Définition du MCD du nouveau template

Il s'agit d'écrire les requêtes SQL qui permettrons de créer les tables vides dans la base de donnée Spatialite.

Plusieurs options :

* Soit ce code SQL est livré dans le standard (ex : GRACE THD)
* Soit il peut être écrit "from scratch" à partir de la définition du modèle (tables/attributs/types)
* Soit il peut être créé "semi-automatiquement" en faisant une intégration en mode CREATE

## Méthode 1 : Code livré dans le standard

Par exemple dans le format Grace THD il s'agit de ces fichiers :

![image](../../static/img/templates_mcd.png)

_Extrait de gracethd_30_tables.sql_

```sql
CREATE TABLE t_adresse( ad_code VARCHAR (254) NOT NULL  ,
 ad_ban_id VARCHAR (24)   ,
 ad_nomvoie VARCHAR (254)   ,
 ad_fantoir VARCHAR (10)   ,
 ad_numero INTEGER   ,
 ad_rep VARCHAR (20)   ,
 ad_insee VARCHAR(6)   ,
 ad_postal VARCHAR(20)   ,
 ad_alias VARCHAR(254)   ,
 ad_nom_ld VARCHAR(254)   ,
 ad_x_ban NUMERIC   ,
 ad_y_ban NUMERIC   ,
 ad_commune VARCHAR (254)   ,
 ad_section VARCHAR (5)   ,
 ad_idpar VARCHAR (20)   ,
 ad_x_parc NUMERIC   ,
        ...
```

Il n'y a rien d'autre à faire que de copier ces fichier a bon endroit (voir plus bas)

----

## Méthode 2 : Code écrit "from scratch"

Il faut avoir un tableau listant le non des tables, le nom des attributs et leur type.

Ensuite, j'utilise généralement la structure suivante en deux fichier :

**_01_exampleMCD_tables.sql_**

```sql
BEGIN;

DROP TABLE IF EXISTS base_prises_bal;
CREATE TABLE base_prises_bal (
    "ogc_fid" INTEGER PRIMARY KEY AUTOINCREMENT,
    'objectid' BIGINT,
    'type' VARCHAR(50),
    'code' VARCHAR(50),
    'adresse' VARCHAR(254),
    'adresse2' VARCHAR(254),
    'commune' VARCHAR(254),
    'codeinsee' VARCHAR(6),
    'codepostal' VARCHAR(6),
    'batiment' VARCHAR(50),
    'nb_pro' INTEGER,  
    'nb_resid' INTEGER,
    'nb_ftte' INTEGER,
    'nb_total' INTEGER,
    'type_adduc' VARCHAR(50),
    'ref_pm' VARCHAR(50),
    'ref_nro' VARCHAR(50),
    'commentair' VARCHAR(254),
    'type_resid' VARCHAR(50),
    'type_pro' VARCHAR(50),
    'x' FLOAT,
    'y' FLOAT,
    'num_projet' VARCHAR(30),
    'statut_dpl' VARCHAR(50),
    'type_relev' VARCHAR(50),
    'cas_partic' VARCHAR(50)
    --"geom" MULTIPOINT
    );

--Structure à répéter pour chaque table.

END;
```

> **NOTA**: Penser à utiliser un tableau avec des formules excel pour générer plus rapidement le code.

Ensuite en spatialite, il faut également déclarer les géométrie de la manière suivante :

**_01_exampleMCD_geom.sql_**

```sql
BEGIN;

SELECT AddGeometryColumn('base_prises_bal','geom',2154,'MULTIPOINT','XY');
SELECT AddGeometryColumn('chambre_a_creer','geom',2154,'MULTIPOINT','XY');
SELECT AddGeometryColumn('ft_chambre_a_utiliser','geom',2154,'MULTIPOINT','XY');
SELECT AddGeometryColumn('chambre_a_utiliser','geom',2154,'MULTIPOINT','XY');
SELECT AddGeometryColumn('ftth_bpe','geom',2154,'MULTIPOINT','XY');
SELECT AddGeometryColumn('pm','geom',2154,'MULTIPOINT','XY');
SELECT AddGeometryColumn('poteaux','geom',2154,'MULTIPOINT','XY');

SELECT AddGeometryColumn('ft_parcours_a_utiliser','geom',2154,'MULTILINESTRING','XY');
SELECT AddGeometryColumn('conduite_a_utiliser','geom',2154,'MULTILINESTRING','XY');
SELECT AddGeometryColumn('ftth_cable','geom',2154,'MULTILINESTRING','XY');
SELECT AddGeometryColumn('ftth_cable_2fo','geom',2154,'MULTILINESTRING','XY');
SELECT AddGeometryColumn('gc_a_creer','geom',2154,'MULTILINESTRING','XY');
SELECT AddGeometryColumn('infra','geom',2154,'MULTILINESTRING','XY');

SELECT AddGeometryColumn('ftth_zone_arriere','geom',2154,'MULTIPOLYGON','XY');

SELECT CreateSpatialIndex('base_prises_bal', 'geom');
SELECT CreateSpatialIndex('chambre_a_creer', 'geom');
SELECT CreateSpatialIndex('ft_chambre_a_utiliser', 'geom');
SELECT CreateSpatialIndex('chambre_a_utiliser', 'geom');
SELECT CreateSpatialIndex('ftth_bpe', 'geom');
SELECT CreateSpatialIndex('pm', 'geom');
SELECT CreateSpatialIndex('poteaux', 'geom');
SELECT CreateSpatialIndex('ft_parcours_a_utiliser', 'geom');
SELECT CreateSpatialIndex('conduite_a_utiliser', 'geom');
SELECT CreateSpatialIndex('ftth_cable', 'geom');
SELECT CreateSpatialIndex('ftth_cable_2fo', 'geom');
SELECT CreateSpatialIndex('gc_a_creer', 'geom');
SELECT CreateSpatialIndex('infra', 'geom');
SELECT CreateSpatialIndex('ftth_zone_arriere', 'geom');

COMMIT;
```

[Voir site de spatialite](https://www.gaia-gis.it/spatialite-3.0.0-BETA/spatialite-cookbook-fr/html/new-geom.html)

----

## Méthode 3 : Méthode semi-automatique

Nous allons générer une première base spatialite en utilisant **import_folder_to_spatialite** à partir d'un livrable exemple

![image](../../static/img/templates_import.png)

Le script crée une base de donnée à la racine du livrable nommée base.sqlite.
En ouvrant la base dans l'éditeur spatialite, il est possible de cliquer sur "**show CREATE statement**"

![image](../../static/img/templates_createstatement.png)

Cela permet de récupérer le code SQL de définition de la table.

Il faut ensuite le copier/coller dans un éditeur de texte pour le nettoyer :

* Insérer des sauts de ligne
* supprimer la clé primaire ogc_fid inutile
* supprimer la colonne de géometrie

Cela permet de générer plus rapidement le fichier
**_01_exampleMCD_tables.sql_** (voir chapitre précédent)

Ensuite le fichier **_01_exampleMCD_geom.sql_** doit être
créé comme dans le chapitre précédent.

----

## Ajout de la table des vues à exporter

Les templates doivent avoir une table qui liste les vues de contrôle à exporter (voir chapitre [restitution des contrôles](/modeles#restitution-des-controles)).

Dans le dossier du MCD, il faut donc ajouter un fichier pour créer cette table :

`99_list_export.sql`

```sql
DROP TABLE IF EXISTS tactis_list_views_to_export;
CREATE TABLE tactis_list_views_to_export (
    view_name VARCHAR(254),
    dest_file VARCHAR(254),
    dest_sheet VARCHAR(254),
    dest_folder VARCHAR(254),
    file_type VARCHAR(254)
    );
```
