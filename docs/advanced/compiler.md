# Compiler un template Quickinteg

!!! warning "Les templates ne sont pas inclus dans le projet Open source"

    Quickinteg est Open source mais les templates sont paramétrés projet par projet et 
    ne sont pas inclus dans la donnée Open source.

    Un template simple paramétré pour effectuer des contrôles élémentaires sur un jeu de 
    donnée **GRACE THD V3 Conteneur 3** est disponible ici à titre d'illustration :

    [Télécharger le template Grace THD v3 Conteneur 3](https://www.dropbox.com/scl/fi/9mwm4zs2ifz2xn26uohs9/template-grace-v3-conteneur3.zip?rlkey=6z3856muzxe7glj6ojljnlbec&dl=1){ .md-button .md-button--primary}

## Différents éléments d'un template

Un template Quickinteg est défini à partir de code source SQL :

- Définition du modèle de donnée, des vues élémentaires
- Points de contrôle
- Paramétrage quickinteg (liste des exports...)

Par ailleurs, Quickinteg propose un framework pour faciliter le développement de points
de contrôle SQL à partir de deux fichiers Excel

- Un catalogue de points de contrôle
- Un fichier de configuration (pour choisir quels points de contrôle activer et leur paramétrage).

Tous ces éléments sont paramétrés dans un fichier `template.conf` permettant de "compiler"
le template avec *Quickinteg*.

```ini title="Exemple de fichier template.conf"
[template]
table_def    = "../sql/gracethd_v3.0"                                         # Dossier contenant les scripts SQL pour créer le modèle de donnée
mcd_check    = {'catalogue' : '../config/gracethd_grille_sql_ctrl_v3.0.xlsx', # Cataloque de points de contrôle
                'config'    : '../config/config_gn.xlsx'}                     # fichier de configuration
sql          = ['../sql/ctrl_synt_generic_grace3.0.sql']                      # liste de fichiers ou dossiers aditionnels contenant du SQL
export_table = '_qi_exports'                                                  # Nom de la table listant les vue à exporter.
```



## Catalogue et configuration de contrôle

Le catalogue de contrôle est un fichier excel listant les points de contrôle disponibles au global.

La configuration de contrôle est un fichier Excel listant les points de contrôle à activer dans le template.

Le développement des points de contrôle est expliqué dans dans le chapitre [Développer des points de contrôle en SQL](./sql_check.md)

Ce qu'il faut comprendre ici c'est que les fichiers seront et assembler pour générer une vue
SQL `v_ctrl_all_source` qui comprends l'ensemble des points de contrôle.

```SQL title="extrait de v_ctrl_all_source"
CREATE VIEW "v_ctrl_all_source" AS
	SELECT 'att_0006' AS ID_Test, "Valeur nulle pour un attribut obligatoire" AS Description, 't_adresse' AS Classe, 'ad_code' AS Attribut, ad_code AS xx_code , "Pas de détail pour cette erreur" AS DETAIL_ERREUR  FROM (SELECT ad_code, ad_code AS Attribut FROM t_adresse) AS tb_ctrl WHERE Attribut IS NULL OR Attribut = '' UNION 
	SELECT 'att_0009' AS ID_Test, "Valeur nulle pour un attribut obligatoire" AS Description, 't_adresse' AS Classe, 'ad_commune' AS Attribut, ad_code AS xx_code , "Pas de détail pour cette erreur" AS DETAIL_ERREUR  FROM (SELECT ad_code, ad_commune AS Attribut FROM t_adresse) AS tb_ctrl WHERE Attribut IS NULL OR Attribut = '' UNION 
	SELECT 'att_0011' AS ID_Test, "Valeur nulle pour un attribut obligatoire" AS Description, 't_adresse' AS Classe, 'ad_datmodi' AS Attribut, ad_code AS xx_code , "Pas de détail pour cette erreur" AS DETAIL_ERREUR  FROM (SELECT ad_code, ad_datmodi AS Attribut FROM t_adresse) AS tb_ctrl WHERE Attribut IS NULL OR Attribut = '' UNION 
...
```

Cette vue retourne un tableau listant toutes les erreurs detectées à partir des points de contrôle 

| id_test   |Description                                    |Classe    | Attribut | xx_code        |DETAIL_ERREUR        |
| :-------- | :-------------------------------------------  | :------- | :------- | :------        | :-------------------|
| list_0008 |Valeur non présente dans la liste l_noeud_type | t_noeud  | nd_type  | ND890000000415 | Valeur nd_type : XX |
| ...       |...                                            | ...      | ...      | ...            | ...                 |


## SQL Additionnel

Les fichiers SQL additionnels peuvent servir à améliorer la présentation. Par exemple
pour avoir une table listant le nombre d'erreur par point de contrôle, on peut ajouter ce 
code :

```sql
CREATE VIEW "v_ctrl_synt" AS
SELECT ID_Test, Description, Classe, Attribut, count(ID_Test) AS 'Nombre'  FROM "v_ctrl_all" GROUP BY ID_Test;
```

## Restitution des contrôles (liste des exports)

Pour que *Quickinteg* puisse restituer les contrôles à l'issue de l'intégration des données, 
il est nécessaire de lister les tables ou vues que l'on souhaite exporter et sous quel format.

Ces tables peuvent être les vues de contrôles ci-dessus (`v_ctrl_all_source0`, `v_ctrl_synt`)
ou toute autre vue SQL paramétrée dans le template.

La table `_qi_exports` listes les différents exports à réaliser en fin d'intégration :

| view_name | file_name| sheet_name | folder | file_type |
|-----------|----------|------------|--------|-----------|
|Nom de la vue/table à exporter| Fichier de destination | Onglet/table de destination | Dossier de destination | Format (xlsx, csv, shp et sqlite)|

```SQL title="SQL pour paramétrer les exports"
CREATE TABLE IF NOT EXISTS _qi_exports (
    view_name VARCHAR(254), 
    dest_file VARCHAR(254), 
    dest_sheet VARCHAR(254),
    dest_folder VARCHAR(254),
    file_type VARCHAR(254)
    );

INSERT INTO _qi_exports 
VALUES 
    /* view_name               file_name           sheet_name             folder               file_type */
    
    ('v_ctrl_all',             'gracethd_v3.0',    '_qi_check_full',       '',                 'sqlite'),
    ('v_ctrl_synt_pourcent',   'gracethd_v3.0',    '_qi_check_synt',       '',                 'sqlite'),
	('_qi_check_full',         '_qi_check',        'erreurs',              '/qi_grace_check',  'xlsx'),
    ('_qi_check_synt',         '_qi_check',        'synthese_erreurs',     '/qi_grace_check',  'xlsx')
    ;

COMMIT;
```

## La compilation d'un template

La compilation d'un template peut se faire au choix avec la commande [`qi-create`](../development/cli.md#documentation-du-cli) ou avec le
script QGIS `Compiler un template`.

![qi-qgis-compile.webp](../static/img/qi-qgis-compile.webp)

La "compilation" d'un template correspond aux étapes suivantes :

1. Une base Spatialite vierge est créée
2. Les fichiers SQL dans le dossiers `../sql/gracethd_v3.0` sont exécutés dans la base
3. Les fichiers `../config/gracethd_grille_sql_ctrl_v3.0.xlsx` et `../config/config_gn.xlsx`
sont utilisés pour générer du SQL correspondant aux vues de contrôle
4. Le fichier `../sql/ctrl_synt_generic_grace3.0.sql` est exécuté sur la base
5. Les vues listées dans la table `_qi_exports` sont testées pour vérifier la validité du template
6. Le template est prêt et peut être utilisé pour un contrôle (voir [Premiers pas](../premiers_pas.md))

??? info "Voir les logs détaillés"

    ```log
    ❯ qi-create template.conf
    INFO :: Lecture du fichier de configuration template.conf
    INFO :: table_def: ./gracethd_v3.0
    INFO :: mcd_check: {'catalogue': './gracethd_grille_sql_ctrl_v3.0-public.xlsx', 'config': './config.xlsx'}
    INFO :: sql: ['./ctrl_synt_generic_grace3.0.sql']
    INFO :: export_table: _qi_exports
    INFO :: Configuration : 
    {'table_def': './gracethd_v3.0', 'mcd_check': {'catalogue': './gracethd_grille_sql_ctrl_v3.0-public.xlsx', 'config': './config.xlsx'}, 'sql': ['./ctrl_synt_generic_grace3.0.sql'], 'export_table': '_qi_exports'}
    INFO :: Le fichier cible est un dossier. Liste des fichiers *.sql trouvés dans ce dossier : 
    INFO :: gracethd_v3.0/gracethd_01_Référentiel_Commun.sql
    INFO :: gracethd_v3.0/gracethd_02_Référentiel_Reseau_Optique.sql
    INFO :: gracethd_v3.0/gracethd_03_Référentiel_Génie_Civil.sql
    INFO :: gracethd_v3.0/gracethd_40_spatialite.sql
    INFO :: gracethd_v3.0/gracethd_50_index.sql
    INFO :: gracethd_v3.0/gracethd_61_vues_elem.sql
    INFO :: Lecture du fichier gracethd_v3.0/gracethd_01_Référentiel_Commun.sql
    INFO :: Connexion à la base de donnée gracethd_v3.0.sqlite
    INFO :: Exécution du script sur la base de donnée
    INFO :: Lecture du fichier gracethd_v3.0/gracethd_02_Référentiel_Reseau_Optique.sql
    INFO :: Connexion à la base de donnée gracethd_v3.0.sqlite
    INFO :: Exécution du script sur la base de donnée
    INFO :: Lecture du fichier gracethd_v3.0/gracethd_03_Référentiel_Génie_Civil.sql
    INFO :: Connexion à la base de donnée gracethd_v3.0.sqlite
    INFO :: Exécution du script sur la base de donnée
    INFO :: Lecture du fichier gracethd_v3.0/gracethd_40_spatialite.sql
    INFO :: Connexion à la base de donnée gracethd_v3.0.sqlite
    INFO :: Exécution du script sur la base de donnée
    INFO :: Lecture du fichier gracethd_v3.0/gracethd_50_index.sql
    INFO :: Connexion à la base de donnée gracethd_v3.0.sqlite
    INFO :: Exécution du script sur la base de donnée
    INFO :: Lecture du fichier gracethd_v3.0/gracethd_61_vues_elem.sql
    INFO :: Connexion à la base de donnée gracethd_v3.0.sqlite
    INFO :: Exécution du script sur la base de donnée
    INFO :: Traduction en SQL de la configuation de contrôle : 
    Catalogue : gracethd_grille_sql_ctrl_v3.0-public.xlsx
    Configuration : config.xlsx

    INFO :: Lecture du catalogue de contrôle depuis le fichier gracethd_grille_sql_ctrl_v3.0-public.xlsx
    INFO :: Lecture de la configuration de contrôle depuis le fichier config.xlsx
    INFO :: Exécution du script sur la base de donnée
    INFO :: Lecture du fichier ctrl_synt_generic_grace3.0.sql
    INFO :: Connexion à la base de donnée gracethd_v3.0.sqlite
    INFO :: Exécution du script sur la base de donnée
    INFO :: Vérification de la validité des tables/vues listées dans la table_qi_exports
    INFO :: v_ctrl_all : OK
    INFO :: v_ctrl_synt_pourcent : OK
    INFO :: _qi_check_full : OK
    INFO :: _qi_check_synt : OK
    ```
