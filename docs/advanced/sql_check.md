# Développer des points de contrôle en SQL

## Formalisme des points de contrôle

Une point de contrôle doit respecter un certain formalisme. Elle retourne 6 colonnes : 

- **id_test** : Identifiant unique du test
- **Description** : description du contrôle (exemple : "Valeur non unique pour un attribut avec une contrainte d'unicité")
- **Classe** : table d'appartenance à l'objet contrôle (exemple : "t_local")
- **Attribut** : Attribut contrôlé (exemple : "lc_codeext")
- **xx_code** : Clé primaire de l'entité en erreur (exemple : "LCIM0000000008" correspondant à une valeur de "lc_code")
- **detail_erreur** : Détail additionnel pour cette erreur (exemple : "Valeur : 163007057931356 / nb occurences : 2")

Exemple : 

| id_test   |Description                                    |Classe    | Attribut | xx_code        |DETAIL_ERREUR        |
| :-------- | :-------------------------------------------  | :------- | :------- | :------        | :-------------------|
| list_0008 |Valeur non présente dans la liste l_noeud_type | t_noeud  | nd_type  | ND890000000415 | Valeur nd_type : XX |
| ...       |...                                            | ...      | ...      | ...            | ...                 |

## Implémentation SQL

La requête SQL est décomposée en différents blocs :

??? example "Une requête principale" 

    ```sql
    SELECT lc_code, code_unic, nb_occ 
    FROM t_local 
    LEFT JOIN (
            SELECT lc_codeext AS code_unic, count(lc_codeext) AS nb_occ 
            FROM t_local 
            GROUP BY lc_codeext 
            HAVING (lc_codeext IS NOT NULL OR lc_codeext != '')
            ) AS tb 
    ON t_local.lc_codeext = tb.code_unic
    ```

??? example "Une condition d'erreur"

    ```sql
    nb_occ > 1
    ```

??? example "Le détail de l'erreur"

    ```sql
    " Valeur : " || tb_ctrl.code_unic || " / nb occurences : " || nb_occ
    ```

La requête finale est composée à partir de ces 3 parties : 

!!! example "Example de requête finale pour un point de contrôle"

    ```sql
    SELECT 
        /* En-tête de la requête */
        "unic_0005" AS ID_Test, 
        "Valeur non unique pour un attribut avec une contrainte d'unicité" AS Description, 
        't_local' AS 'Classe', 
        'lc_codeext' AS Attribut, 
        lc_code AS xx_code,
        /* Fin en-tête */

        /* Détail de l'erreur */ 
        " Valeur : "|| tb_ctrl.code_unic || " / nb occurences : " || nb_occ AS DETAIL_ERREUR 

        FROM 
            /*Début requête principale*/
            (
                
                SELECT lc_code, code_unic, nb_occ 
                FROM t_local 
                LEFT JOIN (
                            SELECT lc_codeext AS code_unic, count(lc_codeext) AS nb_occ 
                            FROM t_local 
                            GROUP BY lc_codeext 
                            HAVING (lc_codeext IS NOT NULL OR lc_codeext != '')
                            ) AS tb 
                ON t_local.lc_codeext = tb.code_unic
            ) AS tb_ctrl 
            /* Fin requête principale */

        /* Condition d'erreur */
        WHERE nb_occ > 1
    ```

## Outils de développement

*Quickinteg* utilise un tableur Excel pour inventorier les différents points de contrôle
(voir [Catalogue et configuration de contrôle](./compiler.md/#catalogue-et-configuration-de-controle)). Le format du
tableur est le suivant :

| id_test  | description                               | classe    | attribut   | xx_code |  requ_princ            | condition          | detail_erreur      |
|----------|-------------------------------------------|-----------|------------|---------|------------------------|--------------------|--------------------|
| att_0001 | Valeur nulle pour un attribut obligatoire | t_adresse | ad_code    | ad_code | SQL Requête principale | Condition d'erreur | Détail additionnel |
| att_0002 | Valeur nulle pour un attribut obligatoire | t_adresse | ad_ban_id  | ad_code | SQL Requête principale | Condition d'erreur | Détail additionnel |
| att_0003 | Valeur nulle pour un attribut obligatoire | t_adresse | ad_nomvoie | ad_code | SQL Requête principale | Condition d'erreur | Détail additionnel |

Lors de la compilation, *Quickinteg* se chargera de construire la requête SQL, morceaux par
morceaux.

Pour faciliter le développement, il est également possible de réaliser l'assemblage des requêtes 
SQL directement dans le tableur Excel avec des formules de concaténation :

![qi-catalogue](../static/img/qi-catalogue.webp)
