# Premiers pas

!!! info Prérequis

    Le présent document part du principe que Quickinteg a été installé dans QGIS suivant la 
    procécure décrite au chapitre [Installation dans QGIS](./installation.md)

## Obtenir un template

Un template est une base de donnée spatialite paramétrée de manière spécifique à Quickinteg afin d'automatiser le contrôle de livrables.
La base de donnée comprend :

- Le modèle de donnée (tables, attributs, relations)
- Les requêtes de contrôle
- Eventuellement un projet QGIS mis en forme.

Un template est spécifique à un projet, ainsi qu'au niveau de livrable (APS, APD, DOE...). Il dépend du modèle de données et des règles d'ingénierie spécifique à chaque projet.

Quickinteg permet d'intégrer des données dans le template puis d'extraire les vues de contrôle.

!!! warning "Les templates ne sont pas inclus dans le projet Open source"

    Quickinteg est Open source mais les templates sont paramétrés projet par projet et 
    ne sont pas inclus dans la donnée Open source.

    Un template simple paramétré pour effectuer des contrôles élémentaires sur un jeu de 
    donnée **GRACE THD V3 Conteneur 3** est disponible ici à titre d'illustration :

    [Télécharger le template Grace THD v3 Conteneur 3](https://www.dropbox.com/scl/fi/9mwm4zs2ifz2xn26uohs9/template-grace-v3-conteneur3.zip?rlkey=6z3856muzxe7glj6ojljnlbec&dl=1){ .md-button .md-button--primary}



## Préparation du dossier de travail

Un templage est une base de donnée initialisée avec le schéma de donnée, mais sans données.

Avant toute intégration il faut donc faire une copie du template (pour conserver un template "vierge" pour les prochaines intégrations).

Voici une arborescence de travail possible :

```bash title="Arborescence avant intégration d'un livrable"
└───SIG
    ├───qi_template_grace_v3           # Template vierge
    │   ├───gracethd_v3.0.qgs
    │   ├───gracethd_v3.0.sqlite
    │   └───...                        # Autres fichiers de configuration du template
    │
    ├───src
    │   └───livrable_ref_xxx           # Livrable Grace THD v3
    │       ├───t_adresse.shp
    │       ├───t_noeud.shp
    │       └───...
    │
    └───integ
        └───integ_ref_xxx              # Copie du template en préparation de
            ├───gracethd_v3.0.qgs      # l'intégration du livrable "xxx"
            └───gracethd_v3.0.sqlite

```

## Intégration et export du rapport d'erreur

Pour intégrer et contrôler un livrable GRACE THD, nous allons exécuter le script 
`Intégrer un livrable dans un template (mode simple)`

![Import simple Qgis](./static/img/qi-qgis-import-simple.webp)

Une fois intégrer, voici l'arborescence obtenue :

```bash title="Arborescence après intégration d'un livrable"
└───SIG
    ├───qi_template_grace_v3           # Template vierge
    │   ├───gracethd_v3.0.qgs
    │   └───gracethd_v3.0.sqlite
    │
    ├───src
    │   └───livrable_ref_xxx           # Livrable Grace THD v3
    │       ├───t_adresse.shp
    │       ├───t_noeud.shp
    │       └───...
    │
    └───integ
        └───integ_ref_xxx    
            ├───qi_grace_check
            │   └───_qi_check.xlsx     # Rapport d'erreur      
            │    
            ├───gracethd_v3.0.qgs      
            ├───gracethd_v3.0.sqlite   # La base de donnée contient les données du livrable xxx
            └───log_integ.txt          # Logs de quickinteg      
```

![Résultats d'un contrôle avec _Quickinteg_](./static/img/gn-quickinteg-result.webp)

## Génération de la route optique

Pour intégrer et contrôler un livrable GRACE THD, nous allons exécuter le script 
`Routes Optiques (mode simple)`

![ROP Simple](./static/img/qi-rop-simple.webp)

Les routes optiques sont générées dans le dossier `ropt` à la racine du dossier d'intégration

