# Installation

## Installation dans QGIS

Les scripts "processings" sont des scripts peuvent être exécutés depuis Qgis via le panneau Processing. 
Ces scripts offrent une interface utilisateur (boîtes de dialogue pour la sélection des inputs) grâce à l'API Qgis.

1. Télécharger le dossier la dernière version de Quickingteg depuis la page des releases (**.zip dans la rubrique _Other_**) :
    - <https://gitlab.com/vlebert/quickinteg/-/releases>
      ![image](static/img/dl_quickinteg.png)

2. Dézipper ensuite Quickinteg dans un répertoire d'installation
    - Choisissez un répertoir local (par exemple `C:\Quickinteg` ou `Mes Documents`). Eviter les lecteurs réseau
    - Idéalement, le repertoire d'extraction sera numéroté avec la version de Quickinteg pour faciliter la mise à jour.
        ```
        c:
        ├───quickinteg_x.x.x
        │   ├─── qgis_processing
        │   ├─── ...
        │   ├─── ...
        │   └─── ...
        ├───quickinteg_x.x.y
        │   ├─── qgis_processing
        │   ├─── ...
        │   ├─── ...
        │   └───
        ```


3. Ajouter à QGIS
    - Dans QGIS, modifier l'option : **`Préférences > Options > Traitement > Scripts > Répertoire des scripts`**
    - Ajouter le dossier **`quickinteg/qgis_processing`** dans la liste des repertoires (le dossier racine)
        ```tree
        ├───quickinteg_x.x.x
        │   ├─── qgis_processing -------> Dossier racine à ajouter dans les options QGIS
        │   ├─── quickinteg
        │   ├─── ...
        │   ├─── ...
        │   └─── ...
        ```
        ![Paramétrage QGIS](static/img/qi-qgis-params.webp)


## Installation en mode standalone (CLI)

Voir [cette page](./development/cli.md)

