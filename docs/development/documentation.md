# Documentation

Project uses MkDocs to generate documentation Markdown files, stored into the `docs` folder in the repository.

!!! info
    The documentation is automatically built and deployed through Gitlab CI to Gitlab Pages.

## Build documentation website

To build it:

```bash
# install aditionnal dependencies
python -m pip install -U -r requirements/documentation.txt
# build it
mkdocs serve
```