# Installation et utilisation de l'interface en ligne de command (CLI)

## Installation

Contrairement à ce qui est décrit sur la page [Environnement de développement](./environment.md), 
pour utiliser le CLI il faut installer les dépéndances non pas dans un environnement virtuel mais
bien dans l'environnement principal de l'OS.

Suivre les étapes indiquer dans [Environnement de développement](./environment.md) (**sauf 
la création de l'environnement virtuel**) ou bien utiliser le script `./cli_install.sh`

```bash title="cli_install.sh"
python3 -m pip install -U -r requirements/embedded.txt --no-deps -t quickinteg/external
python3 -m pip install -e .
```


## Documentation du CLI

Quatre commandes sont disponibles :

- `qi-create` pour créer un template depuis un fichier de configuration
- `qi-import` pour faire une intégration de livrable GRACE THD
- `qi-rop` pour générer la route optique
- `qi-delete` pour supprimer un livrable (versionning)

Pour l'aide utiliser l'option `-h` :

??? info "`qi-create -h`"

    ```
    ❯ qi-create -h
    usage: qi-create [-h] config_file

    positional arguments:
    config_file  Chemin vers le fichier de configuration de template

    ```

??? info "`qi-import -h`"
    ```
    ❯ qi-import -h
    usage: qi-import [-h] [-c] [-d DESTINATION] [-p PREFIX] [-e] [-r] source

    positional arguments:
    source                Chemin vers le repertoir à intégrer

    optional arguments:
    -h, --help            show this help message and exit
    -c, --no-csv          Exclure les fichiers CSV
    -d DESTINATION, --destination DESTINATION
                            Chemin vers une base de donnée existante (mode APPEND).
    -p PREFIX, --prefix PREFIX
                            Préfixe d'importation ajouté sur chaque table.
    -e, --export          Exporter les vues de contrôle
    -r, --recursive       Mode récursif (parcours les sous-dossiers).

    ```

??? info "`qi-rop -h`"
    ```
    ❯ qi-rop -h
    usage: qi-rop [-h] [-v VERSION] db_file

    positional arguments:
    db_file               Base de donnée Quickinteg

    optional arguments:
    -h, --help            show this help message and exit
    -v VERSION, --version VERSION
                            Version GRACE THD (2 ou 3, 3 par défaut)
    ```
??? info "`qi-delete -h`"
    ```
    ❯ qi-delete -h
    usage: qi-delete [-h] db import_prefix

    positional arguments:
    db             Base spatialite dans laquelle supprimer les lignes
    import_prefix  Valeur de l'import prefix
    ```