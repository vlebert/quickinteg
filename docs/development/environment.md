# Environnement de développement

## 1. Cloner le repository Quickinteg

```bash
git clone https://gitlab.com/vlebert/quickinteg.git
```

## 2. Créer l'environnement virtuel

```bash
cd quickinteg
python -m venv .venv 
```

Pour activer : 

```
source .\.venv\Scripts\activate
```

## 3. Installer les dépendances et les outils

```bash
# mettre à jour pip
python -m pip install -U pip

# installer les dépendances
python -m pip install -U -r requirements/base.txt
python -m pip install -U -r requirements/development.txt

# installer pre-commit pour garantir la cohérence et le respect des règles de code communes
pre-commit install
```

## 4. Installer les dépendances tierces

Compte-tenu de la gestion des dépendances tierces dans QGIS, suivre la procédure dédiée : [Installer les dépendances](./external_dependencies.md)