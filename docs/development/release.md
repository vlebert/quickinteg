# Publication d'une nouvelle release

Pour publier une nouvelle release :

1. Mettre à jour le numéro de version dans le fichier `quickinteg/__about__.py`
2. Etiqueter (*tag*) le commit correspondant :
    - soit en ligne de commande. Exemple : `git tag -a 2.5.0 XXXXXXX -m "Version 2.5"` - cf. [doc](https://git-scm.com/book/en/v2/Git-Basics-Tagging)
    - soit via l'interface graphique de GitHub Desktop (clic droit sur le commit dans l'onglet `history`) - cf. [doc](https://docs.github.com/en/free-pro-team@latest/desktop/contributing-and-collaborating-using-github-desktop/managing-tags)
3. Une nouvelle release est automatiquement publiée sur GitLab
4. Complèter la description et éventuellement les autres champs

Les versions publiées (*releases*) se trouvent ici : <https://gitlab.com/vlebert/quickinteg/-/releases>.
