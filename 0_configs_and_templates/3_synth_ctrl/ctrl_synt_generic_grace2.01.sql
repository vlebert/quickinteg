
/*Test pour ajouter r3_code à la liste des résultats*/
DROP VIEW IF EXISTS "v_ctrl_all";
CREATE VIEW "v_ctrl_all" AS
SELECT *
FROM v_ctrl_all_source;

DROP VIEW IF EXISTS "v_ctrl_synt";
CREATE VIEW "v_ctrl_synt" AS
SELECT ID_Test, Description, Classe, Attribut, count(ID_Test) AS 'Nombre_erreurs'  FROM "v_ctrl_all"  GROUP BY ID_Test;


/*jointure avec une table calculée du nombre d'éléments par table*/
DROP VIEW IF EXISTS "v_ctrl_synt_pourcent";
CREATE VIEW "v_ctrl_synt_pourcent" AS
SELECT v_ctrl_synt.*, grace_thd_202rc1.nb AS 'nombre_entitees' , ((v_ctrl_synt.Nombre_erreurs*1.0)/(grace_thd_202rc1.nb*1.0))*100 AS 'Pourcent' FROM "v_ctrl_synt" LEFT JOIN
(SELECT DISTINCT 't_cheminement' AS Classe, (SELECT COUNT(*) FROM t_cheminement) AS 'nb' FROM t_cheminement UNION
SELECT DISTINCT 't_conduite' AS Classe, (SELECT COUNT(*) FROM t_conduite) AS 'nb' FROM t_conduite UNION
SELECT DISTINCT 't_cond_chem' AS Classe, (SELECT COUNT(*) FROM t_cond_chem) AS 'nb' FROM t_cond_chem UNION
SELECT DISTINCT 't_cable' AS Classe, (SELECT COUNT(*) FROM t_cable) AS 'nb' FROM t_cable UNION
SELECT DISTINCT 't_cableline' AS Classe, (SELECT COUNT(*) FROM t_cableline) AS 'nb' FROM t_cableline UNION
SELECT DISTINCT 't_cab_cond' AS Classe, (SELECT COUNT(*) FROM t_cab_cond) AS 'nb' FROM t_cab_cond UNION
SELECT DISTINCT 't_fibre' AS Classe, (SELECT COUNT(*) FROM t_fibre) AS 'nb' FROM t_fibre UNION
SELECT DISTINCT 't_cassette' AS Classe, (SELECT COUNT(*) FROM t_cassette) AS 'nb' FROM t_cassette UNION
SELECT DISTINCT 't_position' AS Classe, (SELECT COUNT(*) FROM t_position) AS 'nb' FROM t_position UNION
SELECT DISTINCT 't_ropt' AS Classe, (SELECT COUNT(*) FROM t_ropt) AS 'nb' FROM t_ropt UNION
SELECT DISTINCT 't_noeud' AS Classe, (SELECT COUNT(*) FROM t_noeud) AS 'nb' FROM t_noeud UNION
SELECT DISTINCT 't_ptech' AS Classe, (SELECT COUNT(*) FROM t_ptech) AS 'nb' FROM t_ptech UNION
SELECT DISTINCT 't_masque' AS Classe, (SELECT COUNT(*) FROM t_masque) AS 'nb' FROM t_masque UNION
SELECT DISTINCT 't_love' AS Classe, (SELECT COUNT(*) FROM t_love) AS 'nb' FROM t_love UNION
SELECT DISTINCT 't_ebp' AS Classe, (SELECT COUNT(*) FROM t_ebp) AS 'nb' FROM t_ebp UNION
SELECT DISTINCT 't_sitetech' AS Classe, (SELECT COUNT(*) FROM t_sitetech) AS 'nb' FROM t_sitetech UNION
SELECT DISTINCT 't_ltech' AS Classe, (SELECT COUNT(*) FROM t_ltech) AS 'nb' FROM t_ltech UNION
SELECT DISTINCT 't_baie' AS Classe, (SELECT COUNT(*) FROM t_baie) AS 'nb' FROM t_baie UNION
SELECT DISTINCT 't_tiroir' AS Classe, (SELECT COUNT(*) FROM t_tiroir) AS 'nb' FROM t_tiroir UNION
SELECT DISTINCT 't_equipement' AS Classe, (SELECT COUNT(*) FROM t_equipement) AS 'nb' FROM t_equipement UNION
SELECT DISTINCT 't_reference' AS Classe, (SELECT COUNT(*) FROM t_reference) AS 'nb' FROM t_reference UNION
SELECT DISTINCT 't_suf' AS Classe, (SELECT COUNT(*) FROM t_suf) AS 'nb' FROM t_suf UNION
SELECT DISTINCT 't_adresse' AS Classe, (SELECT COUNT(*) FROM t_adresse) AS 'nb' FROM t_adresse UNION
SELECT DISTINCT 't_siteemission' AS Classe, (SELECT COUNT(*) FROM t_siteemission) AS 'nb' FROM t_siteemission UNION
SELECT DISTINCT 't_organisme' AS Classe, (SELECT COUNT(*) FROM t_organisme) AS 'nb' FROM t_organisme UNION
SELECT DISTINCT 't_znro' AS Classe, (SELECT COUNT(*) FROM t_znro) AS 'nb' FROM t_znro UNION
SELECT DISTINCT 't_zsro' AS Classe, (SELECT COUNT(*) FROM t_zsro) AS 'nb' FROM t_zsro UNION
SELECT DISTINCT 't_zpbo' AS Classe, (SELECT COUNT(*) FROM t_zpbo) AS 'nb' FROM t_zpbo UNION
SELECT DISTINCT 't_zdep' AS Classe, (SELECT COUNT(*) FROM t_zdep) AS 'nb' FROM t_zdep UNION
SELECT DISTINCT 't_zcoax' AS Classe, (SELECT COUNT(*) FROM t_zcoax) AS 'nb' FROM t_zcoax UNION
SELECT DISTINCT 't_document' AS Classe, (SELECT COUNT(*) FROM t_document) AS 'nb' FROM t_document UNION
SELECT DISTINCT 't_docobj' AS Classe, (SELECT COUNT(*) FROM t_docobj) AS 'nb' FROM t_docobj UNION
SELECT DISTINCT 't_empreinte' AS Classe, (SELECT COUNT(*) FROM t_empreinte) AS 'nb' FROM t_empreinte UNION
SELECT DISTINCT 't_cable_patch201' AS Classe, (SELECT COUNT(*) FROM t_cable_patch201) AS 'nb' FROM t_cable_patch201 UNION
SELECT DISTINCT 't_zpbo_patch201' AS Classe, (SELECT COUNT(*) FROM t_zpbo_patch201) AS 'nb' FROM t_zpbo_patch201 UNION
SELECT DISTINCT 't_cassette_patch201' AS Classe, (SELECT COUNT(*) FROM t_cassette_patch201) AS 'nb' FROM t_cassette_patch201 UNION
SELECT DISTINCT 't_ltech_patch201' AS Classe, (SELECT COUNT(*) FROM t_ltech_patch201) AS 'nb' FROM t_ltech_patch201) AS grace_thd_202rc1
ON v_ctrl_synt.Classe = grace_thd_202rc1.Classe;


/*Création d'une table tactis_list_views_to_export pour définir les exports souhaités. Cette table sera appelée
par le script d'import sous QGIS.
La table tactis_list_views_to_export a pu être crée précédemment (3_synth_ctrl, 4_divers....)
On utilise donc CREATE IF NOT EXISTS*/
CREATE TABLE IF NOT EXISTS tactis_list_views_to_export (
    view_name VARCHAR(254),
    dest_file VARCHAR(254),
    dest_sheet VARCHAR(254),
    dest_folder VARCHAR(254),
    file_type VARCHAR(254)
    );

/*On supprimme les lignes par couple "nom_de_fichier"/"type_de_fichier"
 pour ne pas insérer des lignes en doublons si on exécute plusieurs fois les différents scripts  SQL*/
DELETE FROM tactis_list_views_to_export
WHERE
    dest_file || file_type
    IN ('tactis_grace_checkxlsx');

/*On insère les paramètre d'export souhaités. On peut mettre plusieurs fois le même fichiers en xlsx et mettre des nom d'onglet différents (sheet_name)*/

INSERT INTO tactis_list_views_to_export
VALUES
    /* view_name                                       file_name                        sheet_name                  folder               file_type */

    ('v_ctrl_all' ,                                  'tactis_grace_check',             'erreurs',                     '/tactis_grace_check',   'xlsx'),
    ('v_ctrl_synt_pourcent' ,                        'tactis_grace_check',             'synthese_erreurs',            '/tactis_grace_check',   'xlsx')
    ;
