BEGIN;
/*Création d'une table tactis_list_views_to_export pour définir les exports souhaités. Cette table sera appelée
par le script d'import sous QGIS.
La table tactis_list_views_to_export a pu être crée précédemment (3_synth_ctrl, 4_divers....)
On utilise donc CREATE IF NOT EXISTS*/
CREATE TABLE IF NOT EXISTS tactis_list_views_to_export (
    view_name VARCHAR(254),
    dest_file VARCHAR(254),
    dest_sheet VARCHAR(254),
    dest_folder VARCHAR(254),
    file_type VARCHAR(254)
    );

/*On supprimme les lignes par couple "nom_de_fichier"/"type_de_fichier"
 pour ne pas insérer des lignes en doublons si on exécute plusieurs fois les différents scripts  SQL*/
DELETE FROM tactis_list_views_to_export
WHERE
    dest_file || file_type
    IN ('v_calc_bp_zsro_statusxlsx', 'v_calc_bp_zsro_statusxlsx');

/*On insère les paramètre d'export souhaités. On peut mettre plusieurs fois le même fichiers en xlsx et mettre des nom d'onglet différents (sheet_name)*/
INSERT INTO tactis_list_views_to_export
VALUES
    /* view_name                                       file_name                        sheet_name                  folder               file_type */
    ('v_calc_bp_zsro_status' ,                       'v_calc_bp_zsro_status',                 '',                        '/divers',             'xlsx'),
    ('v_calc_bp_zsro_avct' ,                          'v_calc_bp_zsro_avct',              '',                            '/divers',             'xlsx')
  ;

COMMIT;


