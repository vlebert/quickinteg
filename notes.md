à la base pour gracethd puis finalement appliqué à d'autres routines, notamment l'intégration de fichiers CSV

2 niveaux d'utilisateurs :

- livraison clé en main : base SQLite + outil avec la bonne configuration
- avancé :

## Scénarii

### Livrer un template

Objectif : générer une base vide avec déjà tout le modèle prêt à être utilisé

En entrée, dans le fichier .conf :

- table_def : fichiers SQL de la création de tables (chemin vers le dossier du modèle du client)
- mcd_check : fichiers excel pour sélectionner les contrôles et de configuration des requêtes à effectuer
- sql : vue de contrôle
- export_table : dossier où exporter les rapports de contrôle


### Clé en main


### Consolidation

### Intégration

Objectif : générer un rapport de contrôle de qualité sur le livrable

En entrée :

- un template
- un livrable : un dossier avec des géométries (shapefiles) et des fichiers tabulaires (CSV)

Ils lancent import folder to spatialite :

1. ils pointent sur leur dossier de livrable
2. Option pour mono-livrable
3. Mode APPEND (puisque la base existe déjà) + sélection base de destination
4. Choisir option exporter les rapports
5. Pas de versionning (mode one shot)

Processing :

1. intégration dans la base des modèles de contrôle
2. Appelle les vues unne par une et exporte les résultats en fichiers Excel

En sortie, des fichiers soit CSV/Excel/Shapefiles/SpatiaLite selon l'option choisie dans le template :

1. Etudier plugin Sharing Resources
2. Packaging de processing avec dépendances
3. Plugin ou exe ?
