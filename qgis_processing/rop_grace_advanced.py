# -*- coding: utf-8 -*-

################################################################################
# This file is part of quickinteg.

# quickinteg is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# quickinteg is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with quickinteg.  If not, see <https://www.gnu.org/licenses/>.
################################################################################

### Ajout du dossier racine au path pour permettre leur import. En effet le contenu
### du fichier est exécuté à l'ouverture de QGIS lors du chargement du module
### processing.
from pathlib import Path
import sys

root_module_path = Path(__file__).parents[1]  ## = dossier racine
if str(root_module_path) not in sys.path:
    sys.path.append(str(root_module_path))

external_module_path = (
    Path(__file__).parents[1] / "quickinteg" / "external"
)  ## = dossier external
if str(external_module_path) not in sys.path:
    sys.path.append(str(external_module_path))

################################################################################


from PyQt5.QtCore import QCoreApplication
from qgis.core import (
    QgsProcessingAlgorithm,
    QgsProcessingParameterEnum,
    QgsProcessingParameterNumber,
    QgsProcessingParameterFile,
)
import processing
from shutil import copyfile
import logging
from quickinteg import rop_grace
from quickinteg import processing_logging
import os


class grace_rop_processing(QgsProcessingAlgorithm):
    """
    This is an example algorithm that takes a vector layer and
    creates a new identical one.

    It is meant to be used as an example of how to create your own
    algorithms and explain methods and variables used to do it. An
    algorithm like this will be available in all elements, and there
    is not need for additional work.

    All Processing algorithms should extend the QgsProcessingAlgorithm
    class.
    """

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.

    MODE = "MODE"
    DB_FILE = "DB_FILE"
    ST_ID = "ST_ID"
    CB_ID = "CB_ID"
    BP_ID = "BP_ID"
    TI_ID = "TI_ID"
    NB_IT = "NB_IT"
    CB_LG = "CB_LG"
    GRACE_VERSION = "GRACE_VERSION"

    dict_st_id = {
        0: "st_code",
        1: "st_codeext",
        2: "st_nom",
        3: "lc_code",
        4: "lc_codeext",
        5: "lc_etiquet",
    }

    dict_cb_id = {0: "cb_code", 1: "cb_codeext", 2: "cb_etiquet"}

    dict_bp_id = {0: "bp_code", 1: "bp_codeext", 2: "bp_etiquet"}

    dict_ti_id = {0: "ti_code", 1: "ti_codeext", 2: "ti_etiquet"}

    dict_cb_lg = {0: "cb_lgreel", 1: "cl_long"}

    def tr(self, string):
        """
        Returns a translatable string with the self.tr() function.
        """
        return QCoreApplication.translate("Processing", string)

    def createInstance(self):
        return grace_rop_processing()

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "rop_grace_advanced"

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr("Routes Optiques (mode avancé)")

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr("Quickinteg")

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "Quickinteg"

    def shortHelpString(self):
        """
        Returns a localised short helper string for the algorithm. This string
        should provide a basic description about what the algorithm does and the
        parameters and outputs associated with it..
        """
        return self.tr(
            "Cet utilitaire calcule la route optique depuis une base GRACE THD Spatialite\n"
            "\nLa route optique est calculée par itération à partir des tables t_tiroir, t_ebp, t_cassette, t_position, t_fibre\n"
            '\nLa route optique sera générée dans un sous-dossier "ropt" à la racine de la base de donnée selectionnée.'
        )

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """
        desc = "Indiquer la vertion de GRACE THD de la base à traiter"
        self.addParameter(
            QgsProcessingParameterEnum(
                self.GRACE_VERSION, desc, ["Grace V2.x", "Grace V3.x"], defaultValue=0
            )
        )

        desc = "Choisir l'attribut pour identifier les sites sur la ROP\n"
        desc += "Pour Grace V2, choisir st_xxx, pour Grace V3, choisir lc_xxxx"
        self.addParameter(
            QgsProcessingParameterEnum(
                self.ST_ID, desc, self.dict_st_id.values(), defaultValue=0
            )
        )
        desc = "Choisir l'attribut pour identifier les câbles sur la ROP"
        self.addParameter(
            QgsProcessingParameterEnum(
                self.CB_ID, desc, self.dict_cb_id.values(), defaultValue=0
            )
        )
        desc = "\nChoisir l'attribut pour identifier les boîtes sur la ROP"
        self.addParameter(
            QgsProcessingParameterEnum(
                self.BP_ID, desc, self.dict_bp_id.values(), defaultValue=0
            )
        )
        desc = "\nChoisir l'attribut pour identifier les tiroirs sur la ROP"
        self.addParameter(
            QgsProcessingParameterEnum(
                self.TI_ID, desc, self.dict_ti_id.values(), defaultValue=0
            )
        )
        desc = "\nChoisir l'attribut pour identifier les longueurs de câble sur la ROP"
        self.addParameter(
            QgsProcessingParameterEnum(
                self.CB_LG, desc, self.dict_cb_lg.values(), defaultValue=0
            )
        )
        desc = "\nChoisir le nombre d'itération (15 par défaut). Augmenter la valeur si la route optique générée est incomplète)"
        self.addParameter(
            QgsProcessingParameterNumber(self.NB_IT, desc, defaultValue=15)
        )

        self.addParameter(
            QgsProcessingParameterFile(
                self.DB_FILE,
                "\nSélectionner la base de donnée spatialite (.sqlite) à partir de laquelle caltuler la ROP",
                behavior=QgsProcessingParameterFile.File,
                extension="sqlite",
            )
        )

    def log(self, msg, log_level="info"):
        processing_logging.log(msg, self.logger, log_level)

    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        """
        # Récupération des inputs
        db_file = self.parameterAsString(parameters, self.DB_FILE, context)
        cb_id = self.dict_cb_id[self.parameterAsEnum(parameters, self.CB_ID, context)]
        bp_id = self.dict_bp_id[self.parameterAsEnum(parameters, self.BP_ID, context)]
        ti_id = self.dict_ti_id[self.parameterAsEnum(parameters, self.TI_ID, context)]
        cb_lg = self.dict_cb_lg[self.parameterAsEnum(parameters, self.CB_LG, context)]
        st_id = self.dict_st_id[self.parameterAsEnum(parameters, self.ST_ID, context)]
        # On ajoute +2 pour grace_version car le parameterEnum va renvoyer 0 ou 1 et on veur 2 ou 3
        grace_version = (
            self.parameterAsEnum(parameters, self.GRACE_VERSION, context) + 2
        )

        log_folder = os.path.dirname(db_file)
        log_file = log_folder + "/log_grace_rop.txt"

        # self.logger = logger.init_Logger(console = False, file = log_file, loggerName = 'processing', logLevel = logging.DEBUG)
        self.logger = logging.getLogger("")
        self.logger.handlers = []
        self.logger.setLevel(logging.DEBUG)
        processing_logging.add_file_handler(self.logger, log_file, logging.DEBUG)
        processing_logging.log_in_qgis_processing = True
        processing_logging.log_in_qgis_processing_method = feedback.pushInfo

        rop_grace.param_st_id = st_id
        rop_grace.param_cb_id = cb_id
        rop_grace.param_bp_id = bp_id
        rop_grace.param_ti_id = ti_id
        rop_grace.param_lg_cable = cb_lg
        rop_grace.grace_version = grace_version

        rop_grace.generate_ropt_full(db_file, n_iteration=15)

        self.logger.handlers = []
        processing_logging.log_in_qgis_processing = False
        return {}
