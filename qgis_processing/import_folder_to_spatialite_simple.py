#! python3  # noqa: E265

################################################################################
# This file is part of quickinteg.

# quickinteg is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# quickinteg is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with quickinteg.  If not, see <https://www.gnu.org/licenses/>.
################################################################################

# ############################################################################
# ########## Libraries #############
# ##################################

# standard library
import logging
import platform

import os
from pathlib import Path
import sys

# Ajout du dossier racine au path pour permettre leur import. En effet le contenu
# du fichier est exécuté à l'ouverture de QGIS lors du chargement du module
# processing.
root_module_path = Path(__file__).parents[1]  # dossier racine de mcd_check
if str(root_module_path) not in sys.path:
    sys.path.append(str(root_module_path))
# ###############################################################################

# QGIS
from PyQt5.QtCore import QCoreApplication
from qgis.core import (
    QgsProcessingAlgorithm,
    QgsProcessingParameterEnum,
    QgsProcessingParameterBoolean,
    QgsProcessingParameterString,
    QgsProcessingParameterFile,
    QgsApplication,
    QgsSettings,
)

# package
from quickinteg import spatialiteio, processing_logging


# ############################################################################
# ########## Classes ###############
# ##################################


class Import_folder_to_spatialite(QgsProcessingAlgorithm):
    """
    This is an example algorithm that takes a vector layer and
    creates a new identical one.

    It is meant to be used as an example of how to create your own
    algorithms and explain methods and variables used to do it. An
    algorithm like this will be available in all elements, and there
    is not need for additional work.

    All Processing algorithms should extend the QgsProcessingAlgorithm
    class.
    """

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.

    IN_FOLDER = "IN_FOLDER"
    #    LAYER_NAME = 'LAYER_NAME'
    BASE_NAME = "BASE_NAME"
    PREFIX = "PREFIX"
    OUT_BDD_NAME = "OUT_BDD_NAME"
    EXPORT_VIEWS = "EXPORT_VIEWS"
    RECURSIVE = "RECURSIVE"

    def tr(self, string):
        """
        Returns a translatable string with the self.tr() function.
        """
        return QCoreApplication.translate("Processing", string)

    def createInstance(self):
        return Import_folder_to_spatialite()

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "import_folder_to_spatialite_simple"

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr("Intégrer un livrable dans un template (mode simple)")

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr("Quickinteg")

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "Quickinteg"

    def shortHelpString(self):
        """
        Returns a localised short helper string for the algorithm. This string
        should provide a basic description about what the algorithm does and the
        parameters and outputs associated with it..
        """
        return self.tr(
            "Import de fichiers shp/csv vers une base Spatialite(.sqlite)\n"
            "Les fichier doivent être dans un même dossier, sans caractères spéciaux dans les noms de fichier."
            "\n Le nom des tables de destination sera le nom des fichiers, en minuscule."
            "\n en mode réccursif, il faut une arborescence du type : "
            "\nDOSSIER_CIBLE"
            "\n      ├───REF_LIVRABLE_1"
            "\n      │       ├───xx.shp"
            "\n      │       ├───yy.shp"
            "\n      ├───REF_LIVRABLE_2"
            "\n      │       ├───xx.shp"
            "\n      │       ├───yy.shp"
            "\n      ├───REF_LIVRABLE_N..."
            "\n\nPossibilité de créer une nouvelle base ou d'alimenter une base existante"
        )

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """
        self.addParameter(
            QgsProcessingParameterFile(
                self.IN_FOLDER,
                "Dossier comprenant les fichiers .shp et .csv source",
                behavior=QgsProcessingParameterFile.Folder,
            )
        )

        desc = "\nBase de donnée .sqlite de destination"
        self.addParameter(
            # QgsProcessingParameterString(self.BASE_NAME,'Nom de la base de destination (sera ignoré en mode "append")', defaultValue='base.sqlite')
            QgsProcessingParameterFile(
                self.BASE_NAME,
                desc,
                behavior=QgsProcessingParameterFile.File,
                optional=False,
                extension="sqlite",
            )
        )
        desc = "Exporter les vues de contrôle"
        self.addParameter(
            QgsProcessingParameterBoolean(self.EXPORT_VIEWS, desc, defaultValue=True)
        )

    def log(self, msg, log_level="info"):
        processing_logging.log(msg, self.logger, log_level)

    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        """
        # Fix PATH issue on MACOS
        # Source : https://github.com/qgis/QGIS/blob/master/python/plugins/processing/algs/gdal/GdalUtils.py
        envval = os.getenv("PATH")
        # We need to give some extra hints to get things picked up on OS X
        isDarwin = False
        try:
            isDarwin = platform.system() == "Darwin"
        except IOError:  # https://travis-ci.org/m-kuhn/QGIS#L1493-L1526
            pass
        if isDarwin and os.path.isfile(
            os.path.join(QgsApplication.prefixPath(), "bin", "gdalinfo")
        ):
            # Looks like there's a bundled gdal. Let's use it.
            os.environ["PATH"] = "{}{}{}".format(
                os.path.join(QgsApplication.prefixPath(), "bin"), os.pathsep, envval
            )
            os.environ["DYLD_LIBRARY_PATH"] = os.path.join(
                QgsApplication.prefixPath(), "lib"
            )
        else:
            # Other platforms should use default gdal finder codepath
            settings = QgsSettings()
            path = settings.value("/GdalTools/gdalPath", "")
            if not path.lower() in envval.lower().split(os.pathsep):
                envval += "{}{}".format(os.pathsep, path)
                os.putenv("PATH", envval)

        # Récupération des inputs
        in_folder = self.parameterAsString(parameters, self.IN_FOLDER, context)

        base_name = self.parameterAsString(parameters, self.BASE_NAME, context)
        export_views = self.parameterAsBool(parameters, self.EXPORT_VIEWS, context)

        log_folder = os.path.dirname(base_name) if base_name != "" else in_folder
        log_file = log_folder + "/log_integ.txt"

        # self.logger = logger.init_Logger(console = False, file = log_file, loggerName = 'processing', logLevel = logging.DEBUG)
        self.logger = logging.getLogger("")
        self.logger.handlers = []
        self.logger.setLevel(logging.DEBUG)
        processing_logging.add_file_handler(self.logger, log_file, logging.DEBUG)
        processing_logging.log_in_qgis_processing = True
        processing_logging.log_in_qgis_processing_method = feedback.pushInfo

        # Création de la base de donnée
        # En mode create on crée une base base.sqlite dans le dossier source
        # En mode append on crée une sauvegarde de la base de donnée

        spatialiteio.import_folder_to_spl(
            in_folder,
            ogr_target=base_name,
            include_csv=True,
            ignore_grace_list=True,
            recursive=True,
        )

        if export_views:
            self.log("Export des vues de contrôle", log_level="info")
            spatialiteio.export_preset_tables(base_name)

        self.logger.handlers = []
        processing_logging.log_in_qgis_processing = False
        return {self.OUT_BDD_NAME: base_name}
